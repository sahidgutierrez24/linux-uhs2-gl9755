// SPDX-License-Identifier: GPL-2.0-or-later
/*
 *  linux/drivers/mmc/core/uhs2.c - UHS-II driver
 *
 *  Copyright (C) 2014 Intel Corp, All Rights Reserved.
 *  Copyright (C) 2020 Genesys Logic, Inc.
 *  Authors: Ben Chuang <ben.chuang@genesyslogic.com.tw>
 *  Copyright (C) 2020 Linaro Limited
 *  Author: AKASHI Takahiro <takahiro.akashi@linaro.org>
 */

#include <asm/byteorder.h>
#include <linux/err.h>
#include <linux/slab.h>

#include <linux/mmc/host.h>
#include <linux/mmc/card.h>
#include <linux/mmc/mmc.h>
#include <linux/mmc/core.h>
#include <linux/mmc/sdio.h>
#include <linux/mmc/slot-gpio.h>
#include <linux/mmc/uhs2.h>

#include "uhs2.h"
#include "mmc_ops.h"
#include "sd_ops.h"
#include "core.h"

#define DBG(f, x...) \
	pr_warn("[%s()]: " f, __func__, ## x)

/**
 * uhs2_cmd_assemble - assemble and build up uhs2 command
 * @cmd:	MMC command
 * @uhs2_cmd:	UHS2 command
 * @header:	Value of packet header
 * @arg:	Argument of packet
 * @payload:	Payload of packet
 * @plen:	Payload length
 * @resp:	Buffer for response
 * @resp_len:	Response buffer length
 *
 * resp is inputted outside which should be a variable created by caller
 * so caller should handle it. For SD command, there is no uhs2_resp and
 * response should be stored in resp of mmc_command.
 */
static void uhs2_cmd_assemble(struct mmc_command *cmd,
			      struct uhs2_command *uhs2_cmd,
			      u16 header, u16 arg,
			      u32 *payload, u8 plen, u8 *resp, u8 resp_len)
{
	uhs2_cmd->header = header;
	uhs2_cmd->arg = arg;
	uhs2_cmd->payload = payload;
	uhs2_cmd->payload_len = plen * sizeof(u32);
	uhs2_cmd->packet_len = uhs2_cmd->payload_len + 4;

	cmd->uhs2_cmd = uhs2_cmd;
	cmd->uhs2_resp = resp;
	cmd->uhs2_resp_len = resp_len;

	pr_debug("%s: uhs2_cmd: header=0x%x arg=0x%x\n",
		 __func__, uhs2_cmd->header, uhs2_cmd->arg);
	pr_debug("%s:           payload_len=%d packet_len=%d resp_len=%d\n",
		 __func__, uhs2_cmd->payload_len, uhs2_cmd->packet_len,
		cmd->uhs2_resp_len);
}

static int uhs2_dev_init(struct mmc_host *host)
{
	struct mmc_command cmd = {0};
	struct uhs2_command uhs2_cmd = {};
	u32 cnt;
	u32 dap, gap, gap1;
	u16 header = 0, arg = 0;
	u32 payload[1];
	u8 plen = 1;
	u8 gd = 0, cf = 1;
	u8 resp[6] = {0};
	u8 resp_len = 6;
	int err;

	dap = host->uhs2_caps.dap;
	gap = host->uhs2_caps.gap;

	header = UHS2_NATIVE_PACKET | UHS2_PACKET_TYPE_CCMD;
	arg = ((UHS2_DEV_CMD_DEVICE_INIT & 0xFF) << 8) |
		UHS2_NATIVE_CMD_WRITE |
		UHS2_NATIVE_CMD_PLEN_4B |
		(UHS2_DEV_CMD_DEVICE_INIT >> 8);

	/* need this for some cards */
	cmd.busy_timeout = 1000;

	for (cnt = 0; cnt < 30; cnt++) {
		payload[0] = ((dap & 0xF) << 12) |
			(cf << 11) |
			((gd & 0xF) << 4) |
			(gap & 0xF);

		uhs2_cmd_assemble(&cmd, &uhs2_cmd, header, arg,
				  payload, plen, resp, resp_len);

		DBG("Begin DEVICE_INIT, header=0x%x, arg=0x%x, payload=0x%x.\n",
		    header, arg, payload[0]);

		DBG("Sending DEVICE_INIT. Count = %d\n", cnt);
		err = mmc_wait_for_cmd(host, &cmd, 0);

		if (err) {
			pr_err("%s: %s: UHS2 CMD send fail, err= 0x%x!\n",
			       mmc_hostname(host), __func__, err);
			return -EIO;
		}

		if (IS_ENABLED(CONFIG_MMC_DEBUG)) {
			int i;

			pr_warn("%s: DEVICE_INIT response is: ",
				mmc_hostname(host));
			for (i = 0; i < resp_len; i++)
				pr_warn("0x%x ", resp[i]);
			pr_warn("\n");
		}

		if (resp[3] != (UHS2_DEV_CMD_DEVICE_INIT & 0xFF)) {
			pr_err("%s: DEVICE_INIT response is wrong!\n",
			       mmc_hostname(host));
			return -EIO;
		}

		if (resp[5] & 0x8) {
			DBG("CF is set, device is initialized!\n");
			host->group_desc = gd;
			break;
		}
		gap1 = resp[4] & 0x0F;
		if (gap == gap1)
			gd++;
	}
	if (cnt == 30) {
		pr_err("%s: DEVICE_INIT fail, already 30 times!\n",
		       mmc_hostname(host));
		return -EIO;
	}

	return 0;
}

static int uhs2_enum(struct mmc_host *host)
{
	struct mmc_command cmd = {0};
	struct uhs2_command uhs2_cmd = {};
	u16 header = 0, arg = 0;
	u32 payload[1];
	u8 plen = 1;
	u8 id_f = 0xF, id_l = 0x0;
	u8 resp[8] = {0};
	u8 resp_len = 8;
	int err;

	header = UHS2_NATIVE_PACKET | UHS2_PACKET_TYPE_CCMD;
	arg = ((UHS2_DEV_CMD_ENUMERATE & 0xFF) << 8) |
		UHS2_NATIVE_CMD_WRITE |
		UHS2_NATIVE_CMD_PLEN_4B |
		(UHS2_DEV_CMD_ENUMERATE >> 8);

	payload[0] = (id_f << 4) | id_l;

	DBG("Begin ENUMERATE, header=0x%x, arg=0x%x, payload=0x%x.\n",
	    header, arg, payload[0]);
	uhs2_cmd_assemble(&cmd, &uhs2_cmd, header, arg, payload, plen,
			  resp, resp_len);

	err = mmc_wait_for_cmd(host, &cmd, 0);
	if (err) {
		pr_err("%s: %s: UHS2 CMD send fail, err= 0x%x!\n",
		       mmc_hostname(host), __func__, err);
		return -EIO;
	}

	if (IS_ENABLED(CONFIG_MMC_DEBUG)) {
		int i;

		pr_warn("%s: ENUMERATE response is: ", mmc_hostname(host));
		for (i = 0; i < resp_len; i++)
			pr_warn("0x%x ", resp[i]);
		pr_warn("\n");
	}

	if (resp[3] != (UHS2_DEV_CMD_ENUMERATE & 0xFF)) {
		pr_err("%s: ENUMERATE response is wrong!\n",
		       mmc_hostname(host));
		return -EIO;
	}

	id_f = (resp[4] >> 4) & 0xF;
	id_l = resp[4] & 0xF;
	DBG("id_f = %d, id_l = %d.\n", id_f, id_l);
	DBG("Enumerate Cmd Completed. No. of Devices connected = %d\n",
	    id_l - id_f + 1);
	host->uhs2_dev_prop.node_id = id_f;

	return 0;
}

static int uhs2_config_read(struct mmc_host *host)
{
	struct mmc_command cmd = {0};
	struct uhs2_command uhs2_cmd = {};
	u16 header = 0, arg = 0;
	u32 cap;
	int err;

	DBG("INQUIRY_CFG: read Generic Caps.\n");
	header = UHS2_NATIVE_PACKET |
		 UHS2_PACKET_TYPE_CCMD |
		 host->uhs2_dev_prop.node_id;
	arg = ((UHS2_DEV_CONFIG_GEN_CAPS & 0xFF) << 8) |
		UHS2_NATIVE_CMD_READ |
		UHS2_NATIVE_CMD_PLEN_4B |
		(UHS2_DEV_CONFIG_GEN_CAPS >> 8);

	DBG("Begin INQUIRY_CFG, header=0x%x, arg=0x%x.\n",
	    header, arg);
	/* There is no payload because per spec, there should be
	 * no payload field for read CCMD.
	 * Plen is set in arg. Per spec, plen for read CCMD
	 * represents the len of read data which is assigned in payload
	 * of following RES (p136).
	 */
	uhs2_cmd_assemble(&cmd, &uhs2_cmd, header, arg, NULL, 0, NULL, 0);

	err = mmc_wait_for_cmd(host, &cmd, 0);
	if (err) {
		pr_err("%s: %s: UHS2 CMD send fail, err= 0x%x!\n",
		       mmc_hostname(host), __func__, err);
		return -EIO;
	}

	if (IS_ENABLED(CONFIG_MMC_DEBUG)) {
		int i;

		pr_warn("%s: INQUIRY_CFG generic response is: ",
			mmc_hostname(host));
		for (i = 0; i < 2; i++)
			pr_warn("0x%x ", cmd.resp[i]);
		pr_warn("\n");
	}

	cap = cmd.resp[0];
	DBG("Device Generic Caps (0-31) is: 0x%x.\n", cap);
	host->uhs2_dev_prop.n_lanes = (cap >> UHS2_DEV_CONFIG_N_LANES_POS) &
					UHS2_DEV_CONFIG_N_LANES_MASK;
	host->uhs2_dev_prop.dadr_len = (cap >> UHS2_DEV_CONFIG_DADR_POS) &
					UHS2_DEV_CONFIG_DADR_MASK;
	host->uhs2_dev_prop.app_type = (cap >> UHS2_DEV_CONFIG_APP_POS) &
					UHS2_DEV_CONFIG_APP_MASK;

	DBG("INQUIRY_CFG: read PHY Caps.\n");
	arg = ((UHS2_DEV_CONFIG_PHY_CAPS & 0xFF) << 8) |
		UHS2_NATIVE_CMD_READ |
		UHS2_NATIVE_CMD_PLEN_8B |
		(UHS2_DEV_CONFIG_PHY_CAPS >> 8);

	DBG("Begin INQUIRY_CFG, header=0x%x, arg=0x%x.\n",
	    header, arg);
	uhs2_cmd_assemble(&cmd, &uhs2_cmd, header, arg, NULL, 0, NULL, 0);

	err = mmc_wait_for_cmd(host, &cmd, 0);
	if (err) {
		pr_err("%s: %s: UHS2 CMD send fail, err= 0x%x!\n",
		       mmc_hostname(host), __func__, err);
		return -EIO;
	}

	if (IS_ENABLED(CONFIG_MMC_DEBUG)) {
		int i;

		pr_warn("%s: INQUIRY_CFG PHY response is: ",
			mmc_hostname(host));
		for (i = 0; i < 2; i++)
			pr_warn("0x%x ", cmd.resp[i]);
		pr_warn("\n");
	}

	cap = cmd.resp[0];
	DBG("Device PHY Caps (0-31) is: 0x%x.\n", cap);
	host->uhs2_dev_prop.phy_minor_rev = cap &
					UHS2_DEV_CONFIG_PHY_MINOR_MASK;
	host->uhs2_dev_prop.phy_major_rev = (cap >>
					UHS2_DEV_CONFIG_PHY_MAJOR_POS) &
					UHS2_DEV_CONFIG_PHY_MAJOR_MASK;
	host->uhs2_dev_prop.can_hibernate = (cap >>
					UHS2_DEV_CONFIG_CAN_HIBER_POS) &
					UHS2_DEV_CONFIG_CAN_HIBER_MASK;

	cap = cmd.resp[1];
	DBG("Device PHY Caps (32-63) is: 0x%x.\n", cap);
	host->uhs2_dev_prop.n_lss_sync = cap & UHS2_DEV_CONFIG_N_LSS_SYN_MASK;
	host->uhs2_dev_prop.n_lss_dir = (cap >>
					UHS2_DEV_CONFIG_N_LSS_DIR_POS) &
					UHS2_DEV_CONFIG_N_LSS_DIR_MASK;
	if (host->uhs2_dev_prop.n_lss_sync == 0)
		host->uhs2_dev_prop.n_lss_sync = 16 << 2;
	else
		host->uhs2_dev_prop.n_lss_sync <<= 2;

	if (host->uhs2_dev_prop.n_lss_dir == 0)
		host->uhs2_dev_prop.n_lss_dir = 16 << 3;
	else
		host->uhs2_dev_prop.n_lss_dir <<= 3;

	DBG("INQUIRY_CFG: read LINK-TRAN Caps.\n");
	arg = ((UHS2_DEV_CONFIG_LINK_TRAN_CAPS & 0xFF) << 8) |
		UHS2_NATIVE_CMD_READ |
		UHS2_NATIVE_CMD_PLEN_8B |
		(UHS2_DEV_CONFIG_LINK_TRAN_CAPS >> 8);

	DBG("Begin INQUIRY_CFG, header=0x%x, arg=0x%x.\n",
	    header, arg);
	uhs2_cmd_assemble(&cmd, &uhs2_cmd, header, arg, NULL, 0, NULL, 0);

	err = mmc_wait_for_cmd(host, &cmd, 0);
	if (err) {
		pr_err("%s: %s: UHS2 CMD send fail, err= 0x%x!\n",
		       mmc_hostname(host), __func__, err);
		return -EIO;
	}

	if (IS_ENABLED(CONFIG_MMC_DEBUG)) {
		int i;

		pr_warn("%s: INQUIRY_CFG Link-Tran response is: ",
			mmc_hostname(host));
		for (i = 0; i < 2; i++)
			pr_warn("0x%x ", cmd.resp[i]);
		pr_warn("\n");
	}

	cap = cmd.resp[0];
	DBG("Device LINK-TRAN Caps (0-31) is: 0x%x.\n", cap);
	host->uhs2_dev_prop.link_minor_rev = cap &
					UHS2_DEV_CONFIG_LT_MINOR_MASK;
	host->uhs2_dev_prop.link_major_rev = (cap >>
					UHS2_DEV_CONFIG_LT_MAJOR_POS) &
					UHS2_DEV_CONFIG_LT_MAJOR_MASK;
	host->uhs2_dev_prop.n_fcu = (cap >> UHS2_DEV_CONFIG_N_FCU_POS) &
					UHS2_DEV_CONFIG_N_FCU_MASK;
	host->uhs2_dev_prop.dev_type = (cap >> UHS2_DEV_CONFIG_DEV_TYPE_POS) &
					UHS2_DEV_CONFIG_DEV_TYPE_MASK;
	host->uhs2_dev_prop.maxblk_len = (cap >>
					UHS2_DEV_CONFIG_MAX_BLK_LEN_POS) &
					UHS2_DEV_CONFIG_MAX_BLK_LEN_MASK;

	cap = cmd.resp[1];
	DBG("Device LINK-TRAN Caps (32-63) is: 0x%x.\n", cap);
	host->uhs2_dev_prop.n_data_gap = cap & UHS2_DEV_CONFIG_N_DATA_GAP_MASK;
	if (host->uhs2_dev_prop.n_fcu == 0)
		host->uhs2_dev_prop.n_fcu = 256;

	return 0;
}

static int uhs2_config_write(struct mmc_host *host)
{
	struct mmc_command cmd = {0};
	struct uhs2_command uhs2_cmd = {};
	u16 header = 0, arg = 0;
	u32 nTry;
	u32 payload[2];
	u8 nMinDataGap;
	u8 plen;
	int err;
	u8 resp[5] = {0};
	u8 resp_len = 5;
	/*
	 * must long enough for RECV_SW_DIR == 2;
	 * need lss_dir at least 4 for GL9755 device
	 * max compitable g_lss_dir = 0
	 */
	u32 g_lss_dir = 0;

	DBG("SET_COMMON_CFG: write Generic Settings.\n");
	header = UHS2_NATIVE_PACKET |
		 UHS2_PACKET_TYPE_CCMD | host->uhs2_dev_prop.node_id;
	arg = ((UHS2_DEV_CONFIG_GEN_SET & 0xFF) << 8) |
		UHS2_NATIVE_CMD_WRITE |
		UHS2_NATIVE_CMD_PLEN_8B |
		(UHS2_DEV_CONFIG_GEN_SET >> 8);

	if (host->uhs2_dev_prop.n_lanes == UHS2_DEV_CONFIG_2L_HD_FD &&
	    host->uhs2_caps.n_lanes == UHS2_DEV_CONFIG_2L_HD_FD) {
		/* Support HD */
		DBG("Both Host and device support 2L-HD.\n");
		host->flags |= MMC_UHS2_2L_HD;
		host->uhs2_caps.n_lanes_set = UHS2_DEV_CONFIG_GEN_SET_2L_FD_HD;
		host->uhs2_dev_prop.n_lanes_set =
				UHS2_DEV_CONFIG_GEN_SET_2L_FD_HD;
		nMinDataGap = 1;
	} else {
		/* Only support 2L-FD so far */
		host->flags &= ~MMC_UHS2_2L_HD;
		host->uhs2_caps.n_lanes_set = UHS2_DEV_CONFIG_GEN_SET_2L_FD_HD;
		host->uhs2_dev_prop.n_lanes_set =
				UHS2_DEV_CONFIG_GEN_SET_2L_FD_HD;
		nMinDataGap = 3;
	}

	plen = 2;
	payload[0] = host->uhs2_dev_prop.n_lanes_set <<
		     UHS2_DEV_CONFIG_N_LANES_POS;
	payload[1] = 0;
	payload[0] = cpu_to_be32(payload[0]);
	payload[1] = cpu_to_be32(payload[1]);

	DBG("Begin SET_COMMON_CFG, header=0x%x, arg=0x%x\n", header, arg);
	DBG("UHS2 write Generic Settings %08x %08x\n",
	    payload[0], payload[1]);
	DBG("flags=%08x dev_prop.n_lanes_set=%x host_caps.n_lanes_set=%x\n",
	    host->flags,
	    host->uhs2_dev_prop.n_lanes_set,
	    host->uhs2_caps.n_lanes_set);

	/*
	 * There is no payload because per spec, there should be
	 * no payload field for read CCMD.
	 * Plen is set in arg. Per spec, plen for read CCMD
	 * represents the len of read data which is assigned in payload
	 * of following RES (p136).
	 */
	uhs2_cmd_assemble(&cmd, &uhs2_cmd, header, arg, payload, plen, NULL, 0);

	err = mmc_wait_for_cmd(host, &cmd, 0);
	if (err) {
		pr_err("%s: %s: UHS2 CMD send fail, err= 0x%x!\n",
		       mmc_hostname(host), __func__, err);
		return -EIO;
	}

	DBG("SET_COMMON_CFG: PHY Settings.\n");
	arg = ((UHS2_DEV_CONFIG_PHY_SET & 0xFF) << 8) |
		UHS2_NATIVE_CMD_WRITE |
		UHS2_NATIVE_CMD_PLEN_8B |
		(UHS2_DEV_CONFIG_PHY_SET >> 8);

	for (nTry = 0; nTry < 2; nTry++) {
		plen = 2;

		if (host->uhs2_caps.speed_range ==
			UHS2_DEV_CONFIG_PHY_SET_SPEED_B) {
			host->flags |= MMC_UHS2_SPEED_B;
			host->uhs2_dev_prop.speed_range_set =
				UHS2_DEV_CONFIG_PHY_SET_SPEED_B;
			DBG("set dev_prop.speed_range_set to SPEED_B\n");
		} else {
			host->uhs2_dev_prop.speed_range_set =
				UHS2_DEV_CONFIG_PHY_SET_SPEED_A;
			host->flags &= ~MMC_UHS2_SPEED_B;
			DBG("set dev_prop.speed_range_set to SPEED_A\n");
		}

		payload[0] = host->uhs2_dev_prop.speed_range_set <<
				UHS2_DEV_CONFIG_PHY_SET_SPEED_POS;

		host->uhs2_dev_prop.n_lss_sync_set =
			(min(host->uhs2_dev_prop.n_lss_sync,
			host->uhs2_caps.n_lss_sync) >> 2) &
			UHS2_DEV_CONFIG_N_LSS_SYN_MASK;
		host->uhs2_caps.n_lss_sync_set =
			host->uhs2_dev_prop.n_lss_sync_set;

		if (nTry) {
			host->uhs2_dev_prop.n_lss_dir_set =
				(max(host->uhs2_dev_prop.n_lss_dir,
				host->uhs2_caps.n_lss_dir) >> 3) &
				UHS2_DEV_CONFIG_N_LSS_DIR_MASK;
			host->uhs2_caps.n_lss_dir_set =
				host->uhs2_dev_prop.n_lss_dir_set;
			payload[1] = (host->uhs2_dev_prop.n_lss_dir_set <<
				UHS2_DEV_CONFIG_N_LSS_DIR_POS) |
				host->uhs2_dev_prop.n_lss_sync_set;
		} else {
			host->uhs2_caps.n_lss_dir_set =
				(host->uhs2_dev_prop.n_lss_dir >> 3) &
				UHS2_DEV_CONFIG_N_LSS_DIR_MASK;
			host->uhs2_dev_prop.n_lss_dir_set =
				((host->uhs2_caps.n_lss_dir >> 3) + 1) &
				UHS2_DEV_CONFIG_N_LSS_DIR_MASK;
		}

		if (!g_lss_dir) {
			host->uhs2_dev_prop.n_lss_dir_set = 0;
		} else {
			host->uhs2_dev_prop.n_lss_dir_set =
				max((u8)g_lss_dir,
				    host->uhs2_dev_prop.n_lss_dir_set);
		}

		payload[1] = (host->uhs2_dev_prop.n_lss_dir_set <<
				UHS2_DEV_CONFIG_N_LSS_DIR_POS) |
				host->uhs2_dev_prop.n_lss_sync_set;
		payload[0] = cpu_to_be32(payload[0]);
		payload[1] = cpu_to_be32(payload[1]);

		DBG("UHS2 SET PHY Settings  %08x %08x\n",
		    payload[0], payload[1]);
		DBG("host->flags=%08x dev_prop.speed_range_set=%x\n",
		    host->flags,
		    host->uhs2_dev_prop.speed_range_set);
		DBG("dev_prop.n_lss_sync_set=%x host_caps.n_lss_sync_set=%x\n",
		    host->uhs2_dev_prop.n_lss_sync_set,
		    host->uhs2_caps.n_lss_sync_set);
		DBG("dev_prop.n_lss_dir_set=%x host_caps.n_lss_dir_set=%x\n",
		    host->uhs2_dev_prop.n_lss_dir_set,
		    host->uhs2_caps.n_lss_dir_set);

		DBG("Begin SET_COMMON_CFG header=0x%x arg=0x%x\n",
		    header, arg);
		DBG("\t\tpayload[0]=0x%x payload[1]=0x%x\n",
		    payload[0], payload[1]);

		resp_len = 4;
		memset(resp, 0, sizeof(resp));

		uhs2_cmd_assemble(&cmd, &uhs2_cmd, header, arg,
				  payload, plen, resp, resp_len);

		err = mmc_wait_for_cmd(host, &cmd, 0);
		if (err) {
			pr_err("%s: %s: UHS2 CMD send fail, err= 0x%x!\n",
			       mmc_hostname(host), __func__, err);
			return -EIO;
		}

		if (!(resp[2] & 0x80))
			break;

		DBG("%s: %s: UHS2 SET PHY Settings fail, res= 0x%x!\n",
		    mmc_hostname(host), __func__,  resp[2]);
	}

	DBG("SET_COMMON_CFG: LINK-TRAN Settings.\n");
	arg = ((UHS2_DEV_CONFIG_LINK_TRAN_SET & 0xFF) << 8) |
		UHS2_NATIVE_CMD_WRITE |
		UHS2_NATIVE_CMD_PLEN_8B |
		(UHS2_DEV_CONFIG_LINK_TRAN_SET >> 8);

	plen = 2;

	if (host->uhs2_dev_prop.app_type == UHS2_DEV_CONFIG_APP_SD_MEM)
		host->uhs2_dev_prop.maxblk_len_set =
			UHS2_DEV_CONFIG_LT_SET_MAX_BLK_LEN;
	else
		host->uhs2_dev_prop.maxblk_len_set =
			min(host->uhs2_dev_prop.maxblk_len,
			    host->uhs2_caps.maxblk_len);
	host->uhs2_caps.maxblk_len_set = host->uhs2_dev_prop.maxblk_len_set;

	host->uhs2_dev_prop.n_fcu_set =
		min(host->uhs2_dev_prop.n_fcu,
		    host->uhs2_caps.n_fcu);
	host->uhs2_caps.n_fcu_set = host->uhs2_dev_prop.n_fcu_set;

	host->uhs2_dev_prop.n_data_gap_set =
		max(nMinDataGap, host->uhs2_dev_prop.n_data_gap);

	host->uhs2_caps.n_data_gap_set = host->uhs2_dev_prop.n_data_gap_set;

	host->uhs2_caps.max_retry_set = 3;
	host->uhs2_dev_prop.max_retry_set = host->uhs2_caps.max_retry_set;

	payload[0] = (host->uhs2_dev_prop.maxblk_len_set <<
			UHS2_DEV_CONFIG_MAX_BLK_LEN_POS) |
			(host->uhs2_dev_prop.max_retry_set <<
			UHS2_DEV_CONFIG_LT_SET_MAX_RETRY_POS) |
			(host->uhs2_dev_prop.n_fcu_set <<
			UHS2_DEV_CONFIG_N_FCU_POS);
	payload[1] = host->uhs2_dev_prop.n_data_gap_set;
	payload[0] = cpu_to_be32(payload[0]);
	payload[1] = cpu_to_be32(payload[1]);

	DBG("Begin SET_COMMON_CFG header=0x%x arg=0x%x\n", header, arg);
	DBG("\t\tpayload[0]=0x%x payload[1]=0x%x\n", payload[0], payload[1]);

	uhs2_cmd_assemble(&cmd, &uhs2_cmd, header, arg, payload, plen, NULL, 0);

	err = mmc_wait_for_cmd(host, &cmd, 0);
	if (err) {
		pr_err("%s: %s: UHS2 CMD send fail, err= 0x%x!\n",
		       mmc_hostname(host), __func__, err);
		return -EIO;
	}

	DBG("SET_COMMON_CFG: Set Config Completion.\n");
	arg = ((UHS2_DEV_CONFIG_GEN_SET & 0xFF) << 8) |
		UHS2_NATIVE_CMD_WRITE |
		UHS2_NATIVE_CMD_PLEN_8B |
		(UHS2_DEV_CONFIG_GEN_SET >> 8);

	plen = 2;
	payload[0] = 0;
	payload[1] = UHS2_DEV_CONFIG_GEN_SET_CFG_COMPLETE;
	payload[0] = cpu_to_be32(payload[0]);
	payload[1] = cpu_to_be32(payload[1]);

	DBG("Begin SET_COMMON_CFG, header=0x%x, arg=0x%x, payload[0] = 0x%x.\n",
	    header, arg, payload[0]);
	resp_len = 5;
	memset(resp, 0, sizeof(resp));
	uhs2_cmd_assemble(&cmd, &uhs2_cmd, header, arg, payload, plen,
			  resp, resp_len);

	err = mmc_wait_for_cmd(host, &cmd, 0);
	if (err) {
		pr_err("%s: %s: UHS2 CMD send fail, err= 0x%x!\n",
		       mmc_hostname(host), __func__, err);
		return -EIO;
	}

	/* Set host Config Setting registers */
	if (host->ops->uhs2_set_reg(host, SET_CONFIG)) {
		pr_err("%s: %s: UHS2 SET_CONFIG fail!\n",
		       mmc_hostname(host), __func__);
		return -EIO;
	}

	return 0;
}

static int uhs2_go_dormant(struct mmc_host *host, bool hibernate)
{
	struct mmc_command cmd = {0};
	struct uhs2_command uhs2_cmd = {};
	u16 header = 0, arg = 0;
	u32 payload[1];
	u8 plen = 1;
	int err;

	/* Disable Normal INT */
	if (host->ops->uhs2_set_reg(host, DISABLE_INT)) {
		pr_err("%s: %s: UHS2 DISABLE_INT fail!\n",
		       mmc_hostname(host), __func__);
		return -EIO;
	}

	header = UHS2_NATIVE_PACKET | UHS2_PACKET_TYPE_CCMD |
		host->uhs2_dev_prop.node_id;

	arg = ((UHS2_DEV_CMD_GO_DORMANT_STATE & 0xFF) << 8) |
		UHS2_NATIVE_CMD_WRITE |
		UHS2_NATIVE_CMD_PLEN_4B |
		(UHS2_DEV_CMD_GO_DORMANT_STATE >> 8);

	if (hibernate)
		payload[0] = UHS2_DEV_CMD_DORMANT_HIBER;

	DBG("Begin GO_DORMANT_STATE, header=0x%x, arg=0x%x, payload=0x%x.\n",
	    header, arg, payload[0]);
	uhs2_cmd_assemble(&cmd, &uhs2_cmd, header, arg, payload, plen, NULL, 0);

	err = mmc_wait_for_cmd(host, &cmd, 0);
	if (err) {
		pr_err("%s: %s: UHS2 CMD send fail, err= 0x%x!\n",
		       mmc_hostname(host), __func__, err);
		return -EIO;
	}

	/* Check Dormant State in Present */
	if (host->ops->uhs2_set_reg(host, CHECK_DORMANT)) {
		pr_err("%s: %s: UHS2 GO_DORMANT_STATE fail!\n",
		       mmc_hostname(host), __func__);
		return -EIO;
	}

	host->ops->uhs2_disable_clk(host);

	return 0;
}

static int uhs2_change_speed(struct mmc_host *host)
{
	int err;

	/* Change Speed Range */
	if (host->ops->uhs2_set_reg(host, SET_SPEED_B)) {
		pr_err("%s: %s: UHS2 SET_SPEED fail!\n",
		       mmc_hostname(host), __func__);
		return -EIO;
	}

	err = uhs2_go_dormant(host, false);
	if (err) {
		pr_err("%s: %s: UHS2 GO_DORMANT_STATE fail, err= 0x%x!\n",
		       mmc_hostname(host), __func__, err);
		return -EIO;
	}

	/* restore sd clock */
	mdelay(5);
	host->ops->uhs2_enable_clk(host);

	/* Enable Normal INT */
	if (host->ops->uhs2_set_reg(host, ENABLE_INT)) {
		pr_err("%s: %s: UHS2 ENABLE_INT fail!\n",
		       mmc_hostname(host), __func__);
		return -EIO;
	}

	if (host->ops->uhs2_detect_init(host)) {
		pr_err("%s: %s: uhs2_detect_init() fail!\n",
		       mmc_hostname(host), __func__);
		return -EIO;
	}

	return 0;
}

/**
 * mmc_uhs2_rescan_try_freq - select UHS2 interface
 * @host: MMC host
 * @freq: Bus speed
 *
 * Try to select UHS2 interface and initialize the bus for a given
 * frequency, @freq.
 *
 * Return:	0 on success, non-zero error on failure
 */
int mmc_uhs2_rescan_try_freq(struct mmc_host *host, unsigned int freq)
{
	int err = -EIO;

	host->flags |= MMC_UHS2_SUPPORT;
	host->f_init = freq;

	pr_debug("%s: %s: trying to init card at %u Hz\n",
		 mmc_hostname(host), __func__, host->f_init);

	mmc_power_up(host, 0);
	if (host->ops->uhs2_detect_init(host)) {
		pr_err("%s: fail to detect UHS2!\n", mmc_hostname(host));
		err = UHS2_PHY_INIT_ERR;
		goto init_fail;
	}

	if (uhs2_dev_init(host)) {
		pr_err("%s: UHS2 DEVICE_INIT fail!\n", mmc_hostname(host));
		goto init_fail;
	}

	if (uhs2_enum(host)) {
		pr_err("%s: UHS2 ENUMERATE fail!\n", mmc_hostname(host));
		goto init_fail;
	}

	if (uhs2_config_read(host)) {
		pr_err("%s: UHS2 INQUIRY_CONFIG fail!\n", mmc_hostname(host));
		goto init_fail;
	}

	if (uhs2_config_write(host)) {
		pr_err("%s: UHS2 SET_COMMON_CONFIG fail!\n",
		       mmc_hostname(host));
		goto init_fail;
	}

	mmc_delay(10);

	/* Change to Speed Range B if it is supported */
	if (host->flags & MMC_UHS2_SPEED_B)
		if (uhs2_change_speed(host)) {
			pr_err("%s: UHS2 uhs2_change_speed() fail!\n",
			       mmc_hostname(host));
			goto init_fail;
		}

	host->flags |= MMC_UHS2_INITIALIZED;

	mmc_send_if_cond(host, host->ocr_avail);

	/* On market, only can some SD cards support UHS-II so only call SD
	 * attach process here.
	 */
	if (!(host->caps2 & MMC_CAP2_NO_SD)) {
		err = mmc_attach_sd(host);
		if (!err)
			return 0;
	} else {
		err = -EIO;
	}

init_fail:
	mmc_power_off(host);
	if (host->flags & MMC_UHS2_INITIALIZED)
		host->flags &= ~MMC_UHS2_INITIALIZED;
	host->flags &= ~MMC_UHS2_SUPPORT;

	return err;
}
EXPORT_SYMBOL_GPL(mmc_uhs2_rescan_try_freq);

/**
 * uhs2_prepare_sd_cmd - prepare for SD command packet
 * @host:	MMC host
 * @mrq:	MMC request
 *
 * Initialize and fill in a header and a payload of SD command packet.
 * The caller should allocate uhs2_command in host->cmd->uhs2_cmd in
 * advance.
 *
 * Return:	0 on success, non-zero error on failure
 */
int uhs2_prepare_sd_cmd(struct mmc_host *host, struct mmc_request *mrq)
{
	struct mmc_command *cmd;
	struct uhs2_command *uhs2_cmd;
	u16 header = 0, arg = 0;
	u32 *payload;
	u8 plen = 0;

	cmd = mrq->cmd;
	header = host->uhs2_dev_prop.node_id;
	if ((cmd->flags & MMC_CMD_MASK) == MMC_CMD_ADTC)
		header |= UHS2_PACKET_TYPE_DCMD;
	else
		header |= UHS2_PACKET_TYPE_CCMD;

	arg = cmd->opcode << UHS2_SD_CMD_INDEX_POS;
	if (host->flags & MMC_UHS2_APP_CMD) {
		arg |= UHS2_SD_CMD_APP;
		host->flags &= ~MMC_UHS2_APP_CMD;
	}

	uhs2_cmd = cmd->uhs2_cmd;
	payload = uhs2_cmd->payload;
	plen = 2; /* at the maximum */

	if ((cmd->flags & MMC_CMD_MASK) == MMC_CMD_ADTC &&
	    !cmd->uhs2_tmode0_flag) {
		if (host->flags & MMC_UHS2_2L_HD)
			arg |= UHS2_DCMD_2L_HD_MODE;

		arg |= UHS2_DCMD_LM_TLEN_EXIST;

		if (cmd->data->blocks == 1 &&
		    cmd->data->blksz != 512 &&
		    cmd->opcode != MMC_READ_SINGLE_BLOCK &&
		    cmd->opcode != MMC_WRITE_BLOCK) {
			arg |= UHS2_DCMD_TLUM_BYTE_MODE;
			payload[1] = uhs2_dcmd_convert_msb(cmd->data->blksz);
		} else {
			payload[1] = uhs2_dcmd_convert_msb(cmd->data->blocks);
		}

		if (cmd->opcode == SD_IO_RW_EXTENDED) {
			arg &= ~(UHS2_DCMD_LM_TLEN_EXIST |
				UHS2_DCMD_TLUM_BYTE_MODE |
				UHS2_NATIVE_DCMD_DAM_IO);
			payload[1] = 0;
			plen = 1;
		}
	} else {
		plen = 1;
	}

	payload[0] = uhs2_dcmd_convert_msb(cmd->arg);
	pr_debug("%s: %s: sd_cmd->arg = 0x%x, payload[0]= 0x%x.\n",
		 mmc_hostname(host), __func__, cmd->arg, payload[0]);

	uhs2_cmd_assemble(cmd, uhs2_cmd, header, arg, payload, plen, NULL, 0);

	return 0;
}
EXPORT_SYMBOL_GPL(uhs2_prepare_sd_cmd);
