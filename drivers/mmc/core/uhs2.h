/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 *  driver/mmc/core/uhs2.h - UHS-II driver
 *
 * Header file for UHS-II packets, Host Controller registers and I/O
 * accessors.
 *
 *  Copyright (C) 2014 Intel Corp, All Rights Reserved.
 */
#ifndef MMC_UHS2_H
#define MMC_UHS2_H

#include <linux/mmc/core.h>
#include <linux/mmc/host.h>

#define UHS2_PHY_INIT_ERR	1

int mmc_uhs2_rescan_try_freq(struct mmc_host *host, unsigned int freq);
int uhs2_prepare_sd_cmd(struct mmc_host *host, struct mmc_request *mrq);

#endif /* MMC_UHS2_H */
