// SPDX-License-Identifier: GPL-2.0-or-later
/*
 *  linux/drivers/mmc/host/sdhci_uhs2.c - Secure Digital Host Controller
 *  Interface driver
 *
 *  Copyright (C) 2014 Intel Corp, All Rights Reserved.
 *  Copyright (C) 2020 Genesys Logic, Inc.
 *  Authors: Ben Chuang <ben.chuang@genesyslogic.com.tw>
 *  Copyright (C) 2020 Linaro Limited
 *  Author: AKASHI Takahiro <takahiro.akashi@linaro.org>
 */

#include <linux/delay.h>
#include <linux/module.h>
#include <linux/regulator/consumer.h>

#include <linux/mmc/mmc.h>

#include "sdhci.h"
#include "sdhci-uhs2.h"

#define DRIVER_NAME "sdhci_uhs2"
#define DBG(f, x...) \
	pr_debug(DRIVER_NAME " [%s()]: " f, __func__, ## x)

static void sdhci_clear_set_irqs(struct sdhci_host *host, u32 clear, u32 set)
{
	u32 ier;

	ier = sdhci_readl(host, SDHCI_INT_ENABLE);
	ier &= ~clear;
	ier |= set;
	sdhci_writel(host, ier, SDHCI_INT_ENABLE);
	sdhci_writel(host, ier, SDHCI_SIGNAL_ENABLE);
}

/**
 * sdhci_uhs2_clear_set_irqs - set Error Interrupt Status Enable register
 * @host:	SDHCI host
 * @clear:	bit-wise clear mask
 * @set:	bit-wise set mask
 *
 * Set/unset bits in UHS-II Error Interrupt Status Enable register
 */
void sdhci_uhs2_clear_set_irqs(struct sdhci_host *host, u32 clear, u32 set)
{
	u32 ier;

	ier = sdhci_readl(host, SDHCI_UHS2_ERR_INT_STATUS_EN);
	ier &= ~clear;
	ier |= set;
	sdhci_writel(host, ier, SDHCI_UHS2_ERR_INT_STATUS_EN);
	sdhci_writel(host, ier, SDHCI_UHS2_ERR_INT_SIG_EN);
}
EXPORT_SYMBOL_GPL(sdhci_uhs2_clear_set_irqs);

/**
 * sdhci_uhs2_reset - invoke SW reset
 * @host: SDHCI host
 * @mask: Control mask
 *
 * Invoke SW reset, depending on a bit in @mask and wait for completion.
 */
void sdhci_uhs2_reset(struct sdhci_host *host, u16 mask)
{
	unsigned long timeout;

	if (!(host->mmc->caps & MMC_CAP_UHS2))
		return;

	sdhci_writew(host, mask, SDHCI_UHS2_SW_RESET);

	if (mask & SDHCI_UHS2_SW_RESET_FULL) {
		host->clock = 0;
		/* Reset-all turns off SD Bus Power */
		if (host->quirks2 & SDHCI_QUIRK2_CARD_ON_NEEDS_BUS_ON)
			sdhci_runtime_pm_bus_off(host);
	}

	/* Wait max 100 ms */
	timeout = 10000;

	/* hw clears the bit when it's done */
	while (sdhci_readw(host, SDHCI_UHS2_SW_RESET) & mask) {
		if (timeout == 0) {
			pr_err("%s: %s: Reset 0x%x never completed.\n",
			       __func__, mmc_hostname(host->mmc), (int)mask);
			pr_err("%s: clean reset bit\n",
			       mmc_hostname(host->mmc));
			sdhci_writeb(host, 0, SDHCI_UHS2_SW_RESET);
			return;
		}
		timeout--;
		udelay(10);
	}
}
EXPORT_SYMBOL_GPL(sdhci_uhs2_reset);

static u8 sdhci_calc_timeout_uhs2(struct sdhci_host *host, u8 *cmd_res,
				  u8 *dead_lock)
{
	u8 count;
	unsigned int cmd_res_timeout, dead_lock_timeout, current_timeout;

	/*
	 * If the host controller provides us with an incorrect timeout
	 * value, just skip the check and use 0xE.  The hardware may take
	 * longer to time out, but that's much better than having a too-short
	 * timeout value.
	 */
	if (host->quirks & SDHCI_QUIRK_BROKEN_TIMEOUT_VAL) {
		*cmd_res = 0xE;
		*dead_lock = 0xE;
		return 0xE;
	}

	/* timeout in us */
	cmd_res_timeout = 5 * 1000;
	dead_lock_timeout = 1 * 1000 * 1000;

	/*
	 * Figure out needed cycles.
	 * We do this in steps in order to fit inside a 32 bit int.
	 * The first step is the minimum timeout, which will have a
	 * minimum resolution of 6 bits:
	 * (1) 2^13*1000 > 2^22,
	 * (2) host->timeout_clk < 2^16
	 *     =>
	 *     (1) / (2) > 2^6
	 */
	count = 0;
	current_timeout = (1 << 13) * 1000 / host->timeout_clk;
	while (current_timeout < cmd_res_timeout) {
		count++;
		current_timeout <<= 1;
		if (count >= 0xF)
			break;
	}

	if (count >= 0xF) {
		DBG("%s: Too large timeout 0x%x requested for CMD_RES!\n",
		    mmc_hostname(host->mmc), count);
		count = 0xE;
	}
	*cmd_res = count;

	count = 0;
	current_timeout = (1 << 13) * 1000 / host->timeout_clk;
	while (current_timeout < dead_lock_timeout) {
		count++;
		current_timeout <<= 1;
		if (count >= 0xF)
			break;
	}

	if (count >= 0xF) {
		DBG("%s: Too large timeout 0x%x requested for DEADLOCK!\n",
		    mmc_hostname(host->mmc), count);
		count = 0xE;
	}
	*dead_lock = count;

	return count;
}

static void sdhci_uhs2_set_timeout(struct sdhci_host *host)
{
	u8 cmd_res, dead_lock;

	sdhci_calc_timeout_uhs2(host, &cmd_res, &dead_lock);
	cmd_res |= dead_lock << SDHCI_UHS2_TIMER_CTRL_DEADLOCK_SHIFT;
	sdhci_writeb(host, cmd_res, SDHCI_UHS2_TIMER_CTRL);
}

static void sdhci_uhs2_set_transfer_mode(struct sdhci_host *host,
					 struct mmc_command *cmd)
{
	u16 mode;
	struct mmc_data *data = cmd->data;
	u16 arg;

	if (!data) {
		/* clear Auto CMD settings for no data CMDs */
		arg = cmd->uhs2_cmd->arg;
		if ((((arg & 0xF) << 8) | ((arg >> 8) & 0xFF)) ==
		       UHS2_DEV_CMD_TRANS_ABORT) {
			mode =  0;
		} else {
			mode = sdhci_readw(host, SDHCI_UHS2_TRANS_MODE);
			if (cmd->opcode == MMC_STOP_TRANSMISSION ||
			    cmd->opcode == MMC_ERASE)
				mode |= SDHCI_UHS2_TRNS_WAIT_EBSY;
			else
				/* send status mode */
				if (cmd->opcode == MMC_SEND_STATUS)
					mode = 0;
		}

		if (IS_ENABLED(CONFIG_MMC_DEBUG))
			DBG("UHS2 no data trans mode is 0x%x.\n", mode);

		sdhci_writew(host, mode, SDHCI_UHS2_TRANS_MODE);
		return;
	}

	WARN_ON(!host->data);

	mode = SDHCI_UHS2_TRNS_BLK_CNT_EN | SDHCI_UHS2_TRNS_WAIT_EBSY;
	if (data->flags & MMC_DATA_WRITE)
		mode |= SDHCI_UHS2_TRNS_DATA_TRNS_WRT;

	if (data->blocks == 1 &&
	    data->blksz != 512 &&
	    cmd->opcode != MMC_READ_SINGLE_BLOCK &&
	    cmd->opcode != MMC_WRITE_BLOCK) {
		mode &= ~SDHCI_UHS2_TRNS_BLK_CNT_EN;
		mode |= SDHCI_UHS2_TRNS_BLK_BYTE_MODE;
	}

	if (host->flags & SDHCI_REQ_USE_DMA)
		mode |= SDHCI_UHS2_TRNS_DMA;

	if ((host->mmc->flags & MMC_UHS2_2L_HD) && !cmd->uhs2_tmode0_flag)
		mode |= SDHCI_UHS2_TRNS_2L_HD;

	sdhci_writew(host, mode, SDHCI_UHS2_TRANS_MODE);

	if (IS_ENABLED(CONFIG_MMC_DEBUG))
		DBG("UHS2 trans mode is 0x%x.\n", mode);
}

static void sdhci_uhs2_send_command(struct sdhci_host *host,
				    struct mmc_command *cmd)
{
	int i, j;
	int cmd_reg;

	if (host->mmc->flags & MMC_UHS2_INITIALIZED) {
		if (!cmd->uhs2_cmd) {
			pr_err("%s: fatal error, no uhs2_cmd!\n",
			       mmc_hostname(host->mmc));
			return;
		}
	}

	i = 0;
	sdhci_writel(host,
		     ((u32)cmd->uhs2_cmd->arg << 16) |
				(u32)cmd->uhs2_cmd->header,
		     SDHCI_UHS2_CMD_PACKET + i);
	i += 4;

	/*
	 * Per spec, playload (config) should be MSB before sending out.
	 * But we don't need convert here because had set payload as
	 * MSB when preparing config read/write commands.
	 */
	for (j = 0; j < cmd->uhs2_cmd->payload_len / sizeof(u32); j++) {
		sdhci_writel(host, *(cmd->uhs2_cmd->payload + j),
			     SDHCI_UHS2_CMD_PACKET + i);
		i += 4;
	}

	for ( ; i < SDHCI_UHS2_CMD_PACK_MAX_LEN; i += 4)
		sdhci_writel(host, 0, SDHCI_UHS2_CMD_PACKET + i);

	if (IS_ENABLED(CONFIG_MMC_DEBUG)) {
		DBG("UHS2 CMD packet_len = %d.\n", cmd->uhs2_cmd->packet_len);
		for (i = 0; i < cmd->uhs2_cmd->packet_len; i++)
			DBG("UHS2 CMD_PACKET[%d] = 0x%x.\n", i,
			    sdhci_readb(host, SDHCI_UHS2_CMD_PACKET + i));
	}

	cmd_reg = cmd->uhs2_cmd->packet_len <<
		SDHCI_UHS2_COMMAND_PACK_LEN_SHIFT;
	if ((cmd->flags & MMC_CMD_MASK) == MMC_CMD_ADTC)
		cmd_reg |= SDHCI_UHS2_COMMAND_DATA;
	if (cmd->opcode == MMC_STOP_TRANSMISSION)
		cmd_reg |= SDHCI_UHS2_COMMAND_CMD12;

	/* UHS2 Native ABORT */
	if ((cmd->uhs2_cmd->header & UHS2_NATIVE_PACKET) &&
	    ((((cmd->uhs2_cmd->arg & 0xF) << 8) |
	    ((cmd->uhs2_cmd->arg >> 8) & 0xFF)) == UHS2_DEV_CMD_TRANS_ABORT))
		cmd_reg |= SDHCI_UHS2_COMMAND_TRNS_ABORT;

	/* UHS2 Native DORMANT */
	if ((cmd->uhs2_cmd->header & UHS2_NATIVE_PACKET) &&
	    ((((cmd->uhs2_cmd->arg & 0xF) << 8) |
	     ((cmd->uhs2_cmd->arg >> 8) & 0xFF)) ==
				UHS2_DEV_CMD_GO_DORMANT_STATE))
		cmd_reg |= SDHCI_UHS2_COMMAND_DORMANT;

	DBG("0x%x is set to UHS2 CMD register.\n", cmd_reg);

	sdhci_writew(host, cmd_reg, SDHCI_UHS2_COMMAND);
}

static void sdhci_uhs2_finish_command(struct sdhci_host *host)
{
	int i;
	bool b_read_a0 = 0;

	if (host->mmc->flags & MMC_UHS2_INITIALIZED) {
		u8 resp;
		u8 ecode;

		resp = sdhci_readb(host, SDHCI_UHS2_RESPONSE + 2);
		if (resp & UHS2_RES_NACK_MASK) {
			ecode = (resp >> UHS2_RES_ECODE_POS) &
				UHS2_RES_ECODE_MASK;
			pr_err("%s: NACK is got, ECODE=0x%x.\n",
			       mmc_hostname(host->mmc), ecode);
		}
		b_read_a0 = 1;
	}

	if (host->cmd->uhs2_resp &&
	    host->cmd->uhs2_resp_len &&
	    host->cmd->uhs2_resp_len <= 20) {
		/* Get whole response of some native CCMD, like
		 * DEVICE_INIT, ENUMERATE.
		 */
		for (i = 0; i < host->cmd->uhs2_resp_len; i++)
			host->cmd->uhs2_resp[i] =
				sdhci_readb(host,
					    SDHCI_UHS2_RESPONSE + i);
	} else {
		/* Get SD CMD response and Payload for some read
		 * CCMD, like INQUIRY_CFG.
		 */
		/* Per spec (p136), payload field is divided into
		 * a unit of DWORD and transmission order within
		 * a DWORD is big endian.
		 */
		if (!b_read_a0)
			sdhci_readl(host, SDHCI_UHS2_RESPONSE);
		for (i = 4; i < 20; i += 4) {
			host->cmd->resp[i / 4 - 1] =
				(sdhci_readb(host,
					     SDHCI_UHS2_RESPONSE + i) << 24) |
				(sdhci_readb(host,
					     SDHCI_UHS2_RESPONSE + i + 1)
					<< 16) |
				(sdhci_readb(host,
					     SDHCI_UHS2_RESPONSE + i + 2)
					<< 8) |
				sdhci_readb(host, SDHCI_UHS2_RESPONSE + i + 3);
		}
	}
}

static void sdhci_uhs2_do_set_ios(struct sdhci_host *host, struct mmc_ios *ios)
{
	u8 cmd_res, dead_lock;
	u16 ctrl_2;
	unsigned long flags;

	spin_lock_irqsave(&host->lock, flags);

	/* UHS2 Timeout Control */
	sdhci_calc_timeout_uhs2(host, &cmd_res, &dead_lock);

	/* change to use calculate value */
	cmd_res |= dead_lock << SDHCI_UHS2_TIMER_CTRL_DEADLOCK_SHIFT;

	sdhci_uhs2_clear_set_irqs(host,
				  SDHCI_UHS2_ERR_INT_STATUS_RES_TIMEOUT |
				  SDHCI_UHS2_ERR_INT_STATUS_DEADLOCK_TIMEOUT,
				  0);
	sdhci_writeb(host, cmd_res, SDHCI_UHS2_TIMER_CTRL);
	sdhci_uhs2_clear_set_irqs(host, 0,
				  SDHCI_UHS2_ERR_INT_STATUS_RES_TIMEOUT |
				  SDHCI_UHS2_ERR_INT_STATUS_DEADLOCK_TIMEOUT);

	/* UHS2 timing */
	ctrl_2 = sdhci_readw(host, SDHCI_HOST_CONTROL2);
	if (ios->timing == MMC_TIMING_UHS2)
		ctrl_2 |= SDHCI_CTRL_UHS_2 | SDHCI_CTRL_UHS2_INTERFACE_EN;
	else
		ctrl_2 &= ~(SDHCI_CTRL_UHS_2 | SDHCI_CTRL_UHS2_INTERFACE_EN);
	sdhci_writew(host, ctrl_2, SDHCI_HOST_CONTROL2);

	if (!(host->quirks2 & SDHCI_QUIRK2_PRESET_VALUE_BROKEN))
		sdhci_enable_preset_value(host, true);

	/* Set VDD2 */
	ios->vdd = fls(host->mmc->ocr_avail) - 1;
	ios->vdd2 = fls(host->mmc->ocr_avail_uhs2) - 1;

	sdhci_set_power(host, ios->power_mode, ios->vdd, ios->vdd2);
	udelay(100);

	host->timing = ios->timing;
	sdhci_set_clock(host, host->clock);

	spin_unlock_irqrestore(&host->lock, flags);
}

static int sdhci_uhs2_interface_detect(struct sdhci_host *host)
{
	int timeout = 100;

	udelay(200); /* wait for 200us before check */

	while (!(sdhci_readl(host, SDHCI_PRESENT_STATE) &
		SDHCI_UHS2_IF_DETECT)) {
		if (timeout == 0) {
			pr_warn("%s: not detect UHS2 interface in 200us.\n",
				mmc_hostname(host->mmc));
			sdhci_dumpregs(host);
			return -EIO;
		}
		timeout--;
		mdelay(1);
	}

	/* Enable UHS2 error interrupts */
	sdhci_uhs2_clear_set_irqs(host, SDHCI_INT_ALL_MASK,
				  SDHCI_UHS2_ERR_INT_STATUS_MASK);

	timeout = 150;
	while (!(sdhci_readl(host, SDHCI_PRESENT_STATE) &
		SDHCI_UHS2_LANE_SYNC)) {
		if (timeout == 0) {
			pr_warn("%s: UHS2 Lane sync fail in 150ms.\n",
				mmc_hostname(host->mmc));
			sdhci_dumpregs(host);
			return -EIO;
		}
		timeout--;
		mdelay(1);
	}

	DBG("%s: UHS2 Lane synchronized in UHS2 mode, PHY is initialized.\n",
	    mmc_hostname(host->mmc));
	return 0;
}

static int sdhci_uhs2_init(struct sdhci_host *host)
{
	u16 caps_ptr = 0;
	u32 caps_gen = 0;
	u32 caps_phy = 0;
	u32 caps_tran[2] = {0, 0};
	struct mmc_host *mmc = host->mmc;

	/*
	 * TODO: may add corresponding members in sdhci_host to
	 * keep these caps.
	 */
	caps_ptr = sdhci_readw(host, SDHCI_UHS2_HOST_CAPS_PTR);
	if (caps_ptr < 0x100 || caps_ptr > 0x1FF) {
		pr_err("%s: SDHCI_UHS2_HOST_CAPS_PTR(%d) is wrong.\n",
		       mmc_hostname(mmc), caps_ptr);
		return -ENODEV;
	}
	caps_gen = sdhci_readl(host,
			       caps_ptr + SDHCI_UHS2_HOST_CAPS_GEN_OFFSET);
	caps_phy = sdhci_readl(host,
			       caps_ptr + SDHCI_UHS2_HOST_CAPS_PHY_OFFSET);
	caps_tran[0] = sdhci_readl(host,
				   caps_ptr + SDHCI_UHS2_HOST_CAPS_TRAN_OFFSET);
	caps_tran[1] = sdhci_readl(host,
				   caps_ptr
					+ SDHCI_UHS2_HOST_CAPS_TRAN_1_OFFSET);

	/* General Caps */
	mmc->uhs2_caps.dap = caps_gen & SDHCI_UHS2_HOST_CAPS_GEN_DAP_MASK;
	mmc->uhs2_caps.gap = (caps_gen & SDHCI_UHS2_HOST_CAPS_GEN_GAP_MASK) >>
			     SDHCI_UHS2_HOST_CAPS_GEN_GAP_SHIFT;
	mmc->uhs2_caps.n_lanes = (caps_gen & SDHCI_UHS2_HOST_CAPS_GEN_LANE_MASK)
			>> SDHCI_UHS2_HOST_CAPS_GEN_LANE_SHIFT;
	mmc->uhs2_caps.addr64 =
		(caps_gen & SDHCI_UHS2_HOST_CAPS_GEN_ADDR_64) ? 1 : 0;
	mmc->uhs2_caps.card_type =
		(caps_gen & SDHCI_UHS2_HOST_CAPS_GEN_DEV_TYPE_MASK) >>
		SDHCI_UHS2_HOST_CAPS_GEN_DEV_TYPE_SHIFT;

	/* PHY Caps */
	mmc->uhs2_caps.phy_rev = caps_phy & SDHCI_UHS2_HOST_CAPS_PHY_REV_MASK;
	mmc->uhs2_caps.speed_range =
		(caps_phy & SDHCI_UHS2_HOST_CAPS_PHY_RANGE_MASK)
		>> SDHCI_UHS2_HOST_CAPS_PHY_RANGE_SHIFT;
	mmc->uhs2_caps.n_lss_sync =
		(caps_phy & SDHCI_UHS2_HOST_CAPS_PHY_N_LSS_SYN_MASK)
		>> SDHCI_UHS2_HOST_CAPS_PHY_N_LSS_SYN_SHIFT;
	mmc->uhs2_caps.n_lss_dir =
		(caps_phy & SDHCI_UHS2_HOST_CAPS_PHY_N_LSS_DIR_MASK)
		>> SDHCI_UHS2_HOST_CAPS_PHY_N_LSS_DIR_SHIFT;
	if (mmc->uhs2_caps.n_lss_sync == 0)
		mmc->uhs2_caps.n_lss_sync = 16 << 2;
	else
		mmc->uhs2_caps.n_lss_sync <<= 2;
	if (mmc->uhs2_caps.n_lss_dir == 0)
		mmc->uhs2_caps.n_lss_dir = 16 << 3;
	else
		mmc->uhs2_caps.n_lss_dir <<= 3;

	/* LINK/TRAN Caps */
	mmc->uhs2_caps.link_rev =
		caps_tran[0] & SDHCI_UHS2_HOST_CAPS_TRAN_LINK_REV_MASK;
	mmc->uhs2_caps.n_fcu =
		(caps_tran[0] & SDHCI_UHS2_HOST_CAPS_TRAN_N_FCU_MASK)
		>> SDHCI_UHS2_HOST_CAPS_TRAN_N_FCU_SHIFT;
	if (mmc->uhs2_caps.n_fcu == 0)
		mmc->uhs2_caps.n_fcu = 256;
	mmc->uhs2_caps.host_type =
		(caps_tran[0] & SDHCI_UHS2_HOST_CAPS_TRAN_HOST_TYPE_MASK)
		>> SDHCI_UHS2_HOST_CAPS_TRAN_HOST_TYPE_SHIFT;
	mmc->uhs2_caps.maxblk_len =
		(caps_tran[0] & SDHCI_UHS2_HOST_CAPS_TRAN_BLK_LEN_MASK)
		>> SDHCI_UHS2_HOST_CAPS_TRAN_BLK_LEN_SHIFT;
	mmc->uhs2_caps.n_data_gap =
		caps_tran[1] & SDHCI_UHS2_HOST_CAPS_TRAN_1_N_DATA_GAP_MASK;

	return 0;
}

static int sdhci_uhs2_do_detect_init(struct sdhci_host *host)
{
	unsigned long flags;
	int ret = -EIO;

	DBG("%s: begin UHS2 init.\n", __func__);
	spin_lock_irqsave(&host->lock, flags);

	if (host->ops && host->ops->uhs2_pre_detect_init)
		host->ops->uhs2_pre_detect_init(host);

	if (sdhci_uhs2_interface_detect(host)) {
		pr_warn("%s: cannot detect UHS2 interface.\n",
			mmc_hostname(host->mmc));
		goto out;
	}

	if (sdhci_uhs2_init(host)) {
		pr_warn("%s: UHS2 init fail.\n", mmc_hostname(host->mmc));
		goto out;
	}

	/* Init complete, do soft reset and enable UHS2 error irqs. */
	sdhci_uhs2_reset(host, SDHCI_UHS2_SW_RESET_SD);
	sdhci_uhs2_clear_set_irqs(host, SDHCI_INT_ALL_MASK,
				  SDHCI_UHS2_ERR_INT_STATUS_MASK);
	/*
	 * !!! SDHCI_INT_ENABLE and SDHCI_SIGNAL_ENABLE was cleared
	 * by SDHCI_UHS2_SW_RESET_SD
	 */
	sdhci_writel(host, host->ier, SDHCI_INT_ENABLE);
	sdhci_writel(host, host->ier, SDHCI_SIGNAL_ENABLE);

	ret = 0;
out:
	spin_unlock_irqrestore(&host->lock, flags);
	return ret;
}

static void sdhci_uhs2_set_config(struct sdhci_host *host)
{
	u32 value;
	u16 sdhci_uhs2_set_ptr = sdhci_readw(host, SDHCI_UHS2_SET_PTR);
	u16 sdhci_uhs2_gen_set_reg = (sdhci_uhs2_set_ptr + 0);
	u16 sdhci_uhs2_phy_set_reg = (sdhci_uhs2_set_ptr + 4);
	u16 sdhci_uhs2_tran_set_reg = (sdhci_uhs2_set_ptr + 8);
	u16 sdhci_uhs2_tran_set_1_reg = (sdhci_uhs2_set_ptr + 12);

	/* Set Gen Settings */
	sdhci_writel(host, host->mmc->uhs2_caps.n_lanes_set <<
		SDHCI_UHS2_GEN_SET_N_LANES_POS, sdhci_uhs2_gen_set_reg);

	/* Set PHY Settings */
	value = (host->mmc->uhs2_caps.n_lss_dir_set <<
			SDHCI_UHS2_PHY_SET_N_LSS_DIR_POS) |
		(host->mmc->uhs2_caps.n_lss_sync_set <<
			SDHCI_UHS2_PHY_SET_N_LSS_SYN_POS);
	if (host->mmc->flags & MMC_UHS2_SPEED_B)
		value |= 1 << SDHCI_UHS2_PHY_SET_SPEED_POS;
	sdhci_writel(host, value, sdhci_uhs2_phy_set_reg);

	/* Set LINK-TRAN Settings */
	value = (host->mmc->uhs2_caps.max_retry_set <<
			SDHCI_UHS2_TRAN_SET_RETRY_CNT_POS) |
		(host->mmc->uhs2_caps.n_fcu_set <<
			SDHCI_UHS2_TRAN_SET_N_FCU_POS);
	sdhci_writel(host, value, sdhci_uhs2_tran_set_reg);
	sdhci_writel(host, host->mmc->uhs2_caps.n_data_gap_set,
		     sdhci_uhs2_tran_set_1_reg);
}

static int sdhci_uhs2_check_dormant(struct sdhci_host *host)
{
	int timeout = 100;

	while (!(sdhci_readl(host, SDHCI_PRESENT_STATE) &
		SDHCI_UHS2_IN_DORMANT_STATE)) {
		if (timeout == 0) {
			pr_warn("%s: UHS2 IN_DORMANT fail in 100ms.\n",
				mmc_hostname(host->mmc));
			sdhci_dumpregs(host);
			return -EIO;
		}
		timeout--;
		mdelay(1);
	}
	return 0;
}

static int sdhci_uhs2_do_set_reg(struct sdhci_host *host, enum uhs2_act act)
{
	unsigned long flags;
	int err = 0;
	u16 sdhci_uhs2_set_ptr = sdhci_readw(host, SDHCI_UHS2_SET_PTR);
	u16 sdhci_uhs2_phy_set_reg = (sdhci_uhs2_set_ptr + 4);

	DBG("Begin sdhci_uhs2_set_reg, act %d.\n", act);
	spin_lock_irqsave(&host->lock, flags);

	switch (act) {
	case SET_CONFIG:
		sdhci_uhs2_set_config(host);
		break;
	case ENABLE_INT:
		sdhci_clear_set_irqs(host, 0, SDHCI_INT_CARD_INT);
		break;
	case DISABLE_INT:
		sdhci_clear_set_irqs(host, SDHCI_INT_CARD_INT, 0);
		break;
	case SET_SPEED_B:
		sdhci_writeb(host, 1 << SDHCI_UHS2_PHY_SET_SPEED_POS,
			     sdhci_uhs2_phy_set_reg);
		break;
	case CHECK_DORMANT:
		err = sdhci_uhs2_check_dormant(host);
		break;
	default:
		pr_err("%s: input action %d is wrong!\n",
		       mmc_hostname(host->mmc), act);
		err = -EIO;
		break;
	}

	spin_unlock_irqrestore(&host->lock, flags);
	return err;
}

static void sdhci_uhs2_irq(struct sdhci_host *host)
{
	u32 uhs2mask;
	struct mmc_command *cmd = host->cmd;

	uhs2mask = sdhci_readl(host, SDHCI_UHS2_ERR_INT_STATUS);
	DBG("*** %s got UHS2 interrupt: 0x%08x\n",
	    mmc_hostname(host->mmc), uhs2mask);

	sdhci_writel(host, uhs2mask & SDHCI_UHS2_ERR_INT_STATUS_MASK,
		     SDHCI_UHS2_ERR_INT_STATUS);

	if (!(uhs2mask & SDHCI_UHS2_ERR_INT_STATUS_MASK))
		return;

	if (uhs2mask & SDHCI_UHS2_ERR_INT_STATUS_CMD_MASK) {
		if (!host->cmd) {
			pr_err("%s: Got cmd interrupt 0x%08x but no cmd.\n",
			       mmc_hostname(host->mmc),
			       (unsigned int)uhs2mask);
			sdhci_dumpregs(host);
			return;
		}
		host->cmd->error = -EILSEQ;
		if (uhs2mask & SDHCI_UHS2_ERR_INT_STATUS_RES_TIMEOUT)
			host->cmd->error = -ETIMEDOUT;
	}

	if (uhs2mask & SDHCI_UHS2_ERR_INT_STATUS_DATA_MASK) {
		if (!host->data) {
			pr_err("%s: Got data interrupt 0x%08x but no data.\n",
			       mmc_hostname(host->mmc),
			       (unsigned int)uhs2mask);
			sdhci_dumpregs(host);
			return;
		}

		if (uhs2mask & SDHCI_UHS2_ERR_INT_STATUS_DEADLOCK_TIMEOUT) {
			pr_err("%s: Got deadlock timeout interrupt 0x%08x\n",
			       mmc_hostname(host->mmc),
			       (unsigned int)uhs2mask);
			host->data->error = -ETIMEDOUT;
		} else if (uhs2mask & SDHCI_UHS2_ERR_INT_STATUS_ADMA) {
			pr_err("%s: ADMA error = 0x %x\n",
			       mmc_hostname(host->mmc),
			       sdhci_readb(host, SDHCI_ADMA_ERROR));
			host->data->error = -EIO;
		} else {
			host->data->error = -EILSEQ;
		}
	}

	if (host->data && host->data->error)
		sdhci_finish_data(host);
	else
		sdhci_finish_mrq(host, cmd->mrq);
}

static int sdhci_uhs2_add_host(struct sdhci_host *host, u32 caps1)
{
	struct mmc_host *mmc;
	u32 max_current_caps2;

	if (host->version < SDHCI_SPEC_400)
		return 0;

	mmc = host->mmc;

	/* Support UHS2 */
	if (caps1 & SDHCI_SUPPORT_UHS2) {
		mmc->caps |= MMC_CAP_UHS2;
		mmc->flags |= MMC_UHS2_SUPPORT;
	}

	max_current_caps2 = sdhci_readl(host, SDHCI_MAX_CURRENT_1);

	if ((caps1 & SDHCI_SUPPORT_VDD2_180) &&
	    !max_current_caps2 &&
	    !IS_ERR(mmc->supply.vmmc2)) {
		/* UHS2 - VDD2 */
		int curr = regulator_get_current_limit(mmc->supply.vmmc2);

		if (curr > 0) {
			/* convert to SDHCI_MAX_CURRENT format */
			curr = curr / 1000;  /* convert to mA */
			curr = curr / SDHCI_MAX_CURRENT_MULTIPLIER;
			curr = min_t(u32, curr, SDHCI_MAX_CURRENT_LIMIT);
			max_current_caps2 = curr;
		}
	}

	if (caps1 & SDHCI_SUPPORT_VDD2_180) {
		mmc->ocr_avail_uhs2 |= MMC_VDD2_165_195;
		/*
		 * UHS2 doesn't require this. Only UHS-I bus needs to set
		 * max current.
		 */
		mmc->max_current_180_vdd2 = (max_current_caps2 &
					SDHCI_MAX_CURRENT_VDD2_180_MASK) *
					SDHCI_MAX_CURRENT_MULTIPLIER;
	} else {
		mmc->caps &= ~MMC_CAP_UHS2;
		mmc->flags &= ~MMC_UHS2_SUPPORT;
	}

	return 0;
}

static void sdhci_uhs2_remove_host(struct sdhci_host *host, int dead)
{
	if (!(host->mmc) || !(host->mmc->flags & MMC_UHS2_SUPPORT))
		return;

	if (!dead)
		sdhci_uhs2_reset(host, SDHCI_UHS2_SW_RESET_FULL);

	sdhci_writel(host, 0, SDHCI_UHS2_ERR_INT_STATUS_EN);
	sdhci_writel(host, 0, SDHCI_UHS2_ERR_INT_SIG_EN);
	host->mmc->flags &= ~MMC_UHS2_SUPPORT;
	host->mmc->flags &= ~MMC_UHS2_INITIALIZED;
}

static int __init sdhci_uhs2_host_init(void)
{
	sdhci_uhs2_ops.reset = sdhci_uhs2_reset;
	sdhci_uhs2_ops.set_timeout = sdhci_uhs2_set_timeout;
	sdhci_uhs2_ops.set_transfer_mode = sdhci_uhs2_set_transfer_mode;
	sdhci_uhs2_ops.send_command = sdhci_uhs2_send_command;
	sdhci_uhs2_ops.finish_command = sdhci_uhs2_finish_command;
	sdhci_uhs2_ops.do_set_ios = sdhci_uhs2_do_set_ios;
	sdhci_uhs2_ops.do_detect_init = sdhci_uhs2_do_detect_init;
	sdhci_uhs2_ops.do_set_reg = sdhci_uhs2_do_set_reg;
	sdhci_uhs2_ops.irq = sdhci_uhs2_irq;
	sdhci_uhs2_ops.add_host = sdhci_uhs2_add_host;
	sdhci_uhs2_ops.remove_host = sdhci_uhs2_remove_host;

	return 0;
}
module_init(sdhci_uhs2_host_init);

static void __exit sdhci_uhs2_exit(void)
{
	struct sdhci_uhs2_ops nops = {};

	sdhci_uhs2_ops = nops;
}
module_exit(sdhci_uhs2_exit);

MODULE_AUTHOR("Intel, Genesys Logic, Linaro");
MODULE_DESCRIPTION("MMC UHS-II Support");
MODULE_LICENSE("GPL v2");
